﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace testerApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //SplashScreenPreview();
            Application.Run(new frmMain());
        }
        public static void SplashScreenPreview() {
            SplashScreen.SplashScreen.ShowSplashScreen();
            Application.DoEvents();
            SplashScreen.SplashScreen.SetStatus("Loading Data");
            System.Threading.Thread.Sleep(500);
            SplashScreen.SplashScreen.SetStatus("Retrieving System Information");
            System.Threading.Thread.Sleep(300);
            SplashScreen.SplashScreen.SetStatus("Checking For Available Ports");
            System.Threading.Thread.Sleep(500);
            SplashScreen.SplashScreen.SetStatus("Initializing Streaming Server");
            System.Threading.Thread.Sleep(100);
            SplashScreen.SplashScreen.SetStatus("Initializing Components");
            System.Threading.Thread.Sleep(400);
            SplashScreen.SplashScreen.SetStatus("Firewall Permission is Required");
            System.Threading.Thread.Sleep(900);
            SplashScreen.SplashScreen.SetStatus("Loading Components");
            System.Threading.Thread.Sleep(240);
            SplashScreen.SplashScreen.SetStatus("Server is Started");
            System.Threading.Thread.Sleep(1900);
            SplashScreen.SplashScreen.CloseForm();
        }
    }
}
