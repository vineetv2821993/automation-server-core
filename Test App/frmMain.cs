﻿/****************************************************************
 * This work is original work authored by Vineet Kumar, released *                *
 * This work is provided as is, no guarentees are made as to    *
 * suitability of this work for any specific purpose, use it at *
 * your own risk.                                               *
 * This product is not intended for use in any form except      *
 * learning. The author recommends only using small sections of *
 * code from this project when integrating the attacked         *
 * TcpServer project into your own project.                     *
 * This product is not intended for use for any comercial       *
 * purposes, however it may be used for such purposes.          *
 ****************************************************************/

using rtaNetworking.Streaming;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace testerApp
{
    public partial class    frmMain : Form
    {
        public delegate void invokeDelegate();
        public frmMain()
        {
            WindowsInterface.BotThread.Start();
            InitializeComponent();
            txtLog.Hide();
            labelLog.Hide();
            hideLogsToolStripMenuItem.Enabled = false;
            showLogsToolStripMenuItem.Enabled = true;
            this.Height -= txtLog.Height-panelSystemInfo.Height;
            
            refreshSystemInfo();

            readResolutionInfo();

            currentToolStripMenuItem.Enabled = false;
            mynotifyicon.Visible = false;
            currentToolStripMenuItem.Text = "Current: " + ImageStreamingServer.Height + "x" + ImageStreamingServer.Width;
            _Server = new ImageStreamingServer();
            _Server.Start(ImageStreamingPort);
            InitializeTimer();
        }

        private void readResolutionInfo()
        {
            XmlDocument xmlDoc = new XmlDocument();            
            xmlDoc.Load(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\data.xml");
            ImageStreamingServer.Height = Convert.ToInt32(xmlDoc.DocumentElement.GetElementsByTagName("Height").Item(0).InnerText);
            ImageStreamingServer.Width = Convert.ToInt32(xmlDoc.DocumentElement.GetElementsByTagName("Width").Item(0).InnerText);
        }
        private void saveResolutionInfo()
        {
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)+"\\data.xml");

            //XmlElement _Height = xmlDoc.DocumentElement.FirstChild.;//.GetElementsByTagName("Height");
           // XmlText _Height_Text = xmlDoc.CreateTextNode(ImageStreamingServer.Height.ToString());
            xmlDoc.DocumentElement.FirstChild.InnerText = ImageStreamingServer.Height.ToString();

            //XmlElement _Width = xmlDoc.CreateElement("Width");
           // XmlText _Width_Text = xmlDoc.CreateTextNode(ImageStreamingServer.Width.ToString());
            xmlDoc.DocumentElement.LastChild.InnerText = ImageStreamingServer.Width.ToString();


            xmlDoc.Save(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)+"\\data.xml");
            if(!File.Exists(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)+"\\data.xml"))
            {

                XmlTextWriter textWritter = new XmlTextWriter(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)+"\\data.xml", null);
                textWritter.WriteStartDocument();
                textWritter.WriteStartElement("rootElement");
                textWritter.WriteEndElement();

                textWritter.Close();
            }
        }

        private void btnChangePort_Click(object sender, EventArgs e)
        {
            try
            {
                openTcpPort();
            }
            catch (FormatException)
            {
                MessageBox.Show("Port must be an integer", "Invalid Port", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            catch (OverflowException)
            {
                MessageBox.Show("Port is too large", "Invalid Port", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void openTcpPort()
        {
            tcpServer1.Close();
            tcpServer1.Port = Convert.ToInt32(txtPort.Text);
            txtPort.Text = tcpServer1.Port.ToString();
            tcpServer1.Open();
            if (tcpServer1.IsOpen)
            {

                displayTcpServerStatus();
                startServerToolStripMenuItem.Enabled = false;
                stopServerToolStripMenuItem.Enabled = true;
                txtPort.Enabled = false;
                txtIdleTime.Enabled = false;
                txtAttempts.Enabled = false;
                txtValidateInterval.Enabled = false;
                txtMaxThreads.Enabled = false;
                txtText.Enabled = true;
                btnSend.Enabled = true;
                btnChangePort.Enabled = false;
            }

        }
        private void closeTcpPort()
        {
            startServerToolStripMenuItem.Enabled = true;
            stopServerToolStripMenuItem.Enabled = false;
            txtPort.Enabled = true;
            txtIdleTime.Enabled = true;
            txtAttempts.Enabled = true;
            txtValidateInterval.Enabled = true;
            txtMaxThreads.Enabled = true;
            txtText.Enabled = false;
            btnSend.Enabled = false;
            btnChangePort.Enabled = true;

            tcpServer1.Close();
            displayTcpServerStatus();
        }

        private void displayTcpServerStatus()
        {
            if (tcpServer1.IsOpen)
            {
                lblStatus.Text = "PORT OPEN";
                lblStatus.BackColor = Color.Lime;
            }
            else
            {
                lblStatus.Text = "PORT NOT OPEN";
                lblStatus.BackColor = Color.Red;
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            send();
        }

        private void send()
        {
            string data = "";

            foreach (string line in txtText.Lines)
            {
                data = data + line.Replace("\r", "").Replace("\n", "") + "\r\n";
            }
            data = data.Substring(0, data.Length - 2);

            tcpServer1.Send(data);

            logData(true, data);
        }

        public void logData(bool sent, string text)
        {
            try
            {
                //Executing Commands
                ClientCommand.CheckAndExecute(getCommandInfo(text));
                text = getCommandInfo(text).ToString();
            }
            catch (NullReferenceException e) { }
            finally
            {
                txtLog.Text += "\r\n" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss tt") + (sent ? " SENT:\r\n" : " RECEIVED:\r\n");
                txtLog.Text += text;
                txtLog.Text += "\r\n";

                if (txtLog.Lines.Length > 500)
                {
                    string[] temp = new string[500];
                    Array.Copy(txtLog.Lines, txtLog.Lines.Length - 500, temp, 0, 500);
                    txtLog.Lines = temp;
                }

                txtLog.SelectionStart = txtLog.Text.Length;
                txtLog.ScrollToCaret();
            }

        }
        public static ClientCommand.commandInfo getCommandInfo(String Data){
            ClientCommand.commandInfo Info = new ClientCommand.commandInfo();
            try
            {
               
                Info.CommandType = Data.Substring(0, Data.IndexOf('.'));
                Info.CommandValue = Data.Substring(Data.IndexOf('.') + 1);
                Info.CommandValue = Info.CommandValue.TrimEnd();
            }
            catch (ArgumentOutOfRangeException e) { 
                return null;
            }
            return  Info;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            tcpServer1.Close();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            btnChangePort_Click(null, null);

            timer1.Enabled = true;
        }

        private void tcpServer1_OnDataAvailable(tcpServer.TcpServerConnection connection)
        {
            byte[] data = readStream(connection.Socket);

            if (data != null)
            {
                string dataStr = Encoding.ASCII.GetString(data);

                invokeDelegate del = () =>
                {
                    logData(false, dataStr);
                };
                Invoke(del);

                data = null;
            }
        }

        protected byte[] readStream(TcpClient client)
        {
            NetworkStream stream = client.GetStream();
            if (stream.DataAvailable)
            {
                byte[] data = new byte[client.Available];

                int bytesRead = 0;
                try
                {
                    bytesRead = stream.Read(data, 0, data.Length);
                }
                catch (IOException)
                {
                }

                if (bytesRead < data.Length)
                {
                    byte[] lastData = data;
                    data = new byte[bytesRead];
                    Array.ConstrainedCopy(lastData, 0, data, 0, bytesRead);
                }
                return data;
            }
            return null;
        }

        private void tcpServer1_OnConnect(tcpServer.TcpServerConnection connection)
        {
            invokeDelegate setText = () => lblConnected.Text = tcpServer1.Connections.Count.ToString();

            Invoke(setText);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            displayTcpServerStatus();
            lblConnected.Text = tcpServer1.Connections.Count.ToString();
        }

        private void txtIdleTime_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int time = Convert.ToInt32(txtIdleTime.Text);
                tcpServer1.IdleTime = time;
            }
            catch (FormatException) { }
            catch (OverflowException) { }
        }

        private void txtMaxThreads_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int threads = Convert.ToInt32(txtMaxThreads.Text);
                tcpServer1.MaxCallbackThreads = threads;
            }
            catch (FormatException) { }
            catch (OverflowException) { }
        }

        private void txtAttempts_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int attempts = Convert.ToInt32(txtAttempts.Text);
                tcpServer1.MaxSendAttempts = attempts;
            }
            catch (FormatException) { }
            catch (OverflowException) { }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Enabled = false;
        }

        private void txtValidateInterval_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int interval = Convert.ToInt32(txtValidateInterval.Text);
                tcpServer1.VerifyConnectionInterval = interval;
            }
            catch (FormatException) { }
            catch (OverflowException) { }
        }

        private void startServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                openTcpPort();
            }
            catch (FormatException)
            {
                MessageBox.Show("Port must be an integer", "Invalid Port", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            catch (OverflowException)
            {
                MessageBox.Show("Port is too large", "Invalid Port", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void stopServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                closeTcpPort();
            }
            catch (Exception)
            {
                displayTcpServerStatus();
            }
        }

        private void hideLogsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtLog.Hide();
            labelLog.Hide();
            panelSystemInfo.Show();
            hideLogsToolStripMenuItem.Enabled = false;
            showLogsToolStripMenuItem.Enabled = true;
            this.Height -= txtLog.Height - panelSystemInfo.Height;
        }

        private void showLogsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtLog.Show();
            labelLog.Show();
            panelSystemInfo.Hide();
            hideLogsToolStripMenuItem.Enabled = true;
            showLogsToolStripMenuItem.Enabled = false;
            this.Height += txtLog.Height - panelSystemInfo.Height;
        }

        private void refreshSystemInfo() {
            labelInfo.Text = "System Information\n";
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                localIP = ip.ToString();
                labelInfo.Text += "\n\n Address Family = " + ip.AddressFamily + ", IP Address = " + localIP;
            }
            String publicIP = "Not Available";
            try {
                publicIP = GetPublicIpAddress();
            }catch(Exception e){
                publicIP = "Not Available";
            }
            labelInfo.Text += "\n\n Public IP Address = " + publicIP;
            /*foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                     //labelInfo.Text +="\n"+ni.Name+ni;
                    foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            labelInfo.Text +=" "+ip.Address.ToString();
                        }
                    }
                }
            }
             * */

        }
        private void systemInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            refreshSystemInfo();
        }

        private void logsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtLog.Clear();
        }

        private void broadcastTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtText.Clear();
        }

        private void aboutAirVoiceControlServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Automation Project Softwares\n©2014 AKS Projects\n Version 1.0.2.2014", "About Automation Server", MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
        }

        private void technicalSupportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Mail Development Team?", "Technical Support", MessageBoxButtons.YesNo, MessageBoxIcon.Question ) == DialogResult.Yes)
            {
                System.Diagnostics.Process.Start("mailto:vineet.kumar.cer11@iitbhu.ac.in");
            }
        }

        private ImageStreamingServer _Server;
        private const int ImageStreamingPort = 12120;
        


        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://localhost:12120/");
        }

        private void changeHeightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ImageStreamingServer.Height = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("- Please input integers only.\n- Effect will take place after restarting the application.", "Enter Height", ImageStreamingServer.Height.ToString(), -1, -1));
            }
            catch (Exception)
            {
                MessageBox.Show("Height must be an integer", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            currentToolStripMenuItem.Text = "Current: "+ImageStreamingServer.Height+"x"+ImageStreamingServer.Width;
            saveResolutionInfo();
        }

        private void changeWidthToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ImageStreamingServer.Width = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("- Please input integers only.\n- Effect will take place after restarting the application.", "Enter Height", ImageStreamingServer.Width.ToString(), -1, -1));
            }
            catch (Exception)
            {
                MessageBox.Show("Width must be an integer", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
        

            }
            currentToolStripMenuItem.Text = "Current: " + ImageStreamingServer.Height + "x" + ImageStreamingServer.Width;
            saveResolutionInfo();

        }

        private void restartServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessStartInfo Info = new ProcessStartInfo();
            Info.Arguments = "/C ping 127.0.0.1 -n 2 && \"" + Application.ExecutablePath + "\"";
            Info.WindowStyle = ProcessWindowStyle.Hidden;
            Info.CreateNoWindow = true;
            Info.FileName = "cmd.exe";
            Process.Start(Info);
            Application.Exit(); 
        }
        private void minimizeToTrayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mynotifyicon.Visible = true;
            mynotifyicon.BalloonTipText = "Server is running in background";
            mynotifyicon.BalloonTipTitle = "Automation Server";
            mynotifyicon.BalloonTipIcon = ToolTipIcon.Info;
            mynotifyicon.ShowBalloonTip(500);
            this.Hide();
        }

        private void mynotifyicon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            mynotifyicon.Visible = false;
            this.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void registerOnlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                auto_id = Microsoft.VisualBasic.Interaction.InputBox("- You must Connected to Internet to register our online service.", "Enter Automation ID", auto_id, -1, -1);
                auto_key = Microsoft.VisualBasic.Interaction.InputBox("- You must Connected to Internet to register our online service.", "Enter Automation Pass Key", auto_key, -1, -1);
                var url = @"http://regnertoolserver.appspot.com/desktop";
                var nvc = new System.Collections.Specialized.NameValueCollection();
                nvc.Add("email", auto_id);
                nvc.Add("pwd", auto_key);
                is_registered_once = true;
                nvc.Add("ip_addr", GetPublicIpAddress());
                var client = new System.Net.WebClient();
                var data = client.UploadValues(url, nvc);
                var res = System.Text.Encoding.ASCII.GetString(data);
                Console.WriteLine(res);
                Console.ReadLine();
                is_registered_once = true;
            }
            catch (Exception)
            {
                MessageBox.Show("Error!", "Communication Problem", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

        }
        private string GetPublicIpAddress()
        {
            var request = (HttpWebRequest)WebRequest.Create("http://ifconfig.me");

            request.UserAgent = "curl"; // this simulate curl linux command

            string publicIPAddress;

            request.Method = "GET";
            using (WebResponse response = request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    publicIPAddress = reader.ReadToEnd();
                }
            }

            return publicIPAddress.Replace("\n", "");
        }

        private void timerInteretSender_Tick(object sender, EventArgs e)
        {
            if (is_registered_once == true) {
                try
                {

                    var url = @"http://regnertoolserver.appspot.com/desktop";
                    var nvc = new System.Collections.Specialized.NameValueCollection();
                    nvc.Add("email", auto_id);
                    nvc.Add("pwd", auto_key);
                    nvc.Add("ip_addr", GetPublicIpAddress());
                    var client = new System.Net.WebClient();
                    var data = client.UploadValues(url, nvc);
                    var res = System.Text.Encoding.ASCII.GetString(data);
                    Console.WriteLine(res);
                    Console.ReadLine();
                }
                catch (Exception)
                {
                    MessageBox.Show("Error!", "Communication Problem", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }

        }
        private void InitializeTimer()
        {
            try{
                timerInteretSender.Interval = 300000;
                // Enable timer.
                 timerInteretSender.Enabled = true;
            }
            catch (Exception)
            {
                MessageBox.Show("Error!", "Communication Problem", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }
    }
}
