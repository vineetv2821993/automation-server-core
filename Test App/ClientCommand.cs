﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace testerApp
{
    public class ClientCommand
    {
        public class commandInfo
        {
            public string CommandType { set; get; }
            public string CommandValue { set; get; }
            public override string ToString() {
                return "Command Information: Type = '" + CommandType + "' Value = '" + CommandValue+"'";
            }
        }
        public static commandInfo CommandInfo = new commandInfo();

        public static void CheckAndExecute(commandInfo info)
        {
            if (info.CommandType == CommandInfoLib.CommandType.Touchpad.ToString())
            {
                WindowsInterface.moveCursor(Convert.ToInt32(info.CommandValue.Substring(0, info.CommandValue.IndexOf(' '))), Convert.ToInt32(info.CommandValue.Substring(info.CommandValue.IndexOf(' ') + 1)));
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.Keyboard.ToString())
            {
                WindowsInterface.sendKeyPress(info.CommandValue);    
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.CDRom.ToString())
            {
                WindowsInterface.setCDRomStatus(info.CommandValue);
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.Brightness.ToString())
            {
                WindowsInterface.setBrightnessLevel(Convert.ToInt32(info.CommandValue));
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.MasterVolume.ToString())
            {
                //WindowsInterface.setVolumeLevel(Convert.ToInt32(info.CommandValue));
                WindowsInterface.setVolumeLevel(info.CommandValue);
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.BrightnessScreenToggle.ToString())
            {
                WindowsInterface.screenStatus(info.CommandValue);
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.MasterVolumeMuteToggle.ToString())
            {
                WindowsInterface.masterVolumeStatus(info.CommandValue);
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.Clipboard.ToString())
            {
                WindowsInterface.clipboardStatus(info.CommandValue);
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.Connect.ToString())
            {
                WindowsInterface.clientConnected();
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.ExecutePlayer.ToString())
            {
                WindowsInterface.executePlayer();
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.Console.ToString())
            {
                WindowsInterface.consoleExecute(info.CommandValue);
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.ConsoleTool.ToString())
            {
                WindowsInterface.consoleToolExecute(info.CommandValue);
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.Resolution.ToString())
            {
                /*
                int width = Convert.ToInt32(info.CommandType.Substring(0,info.CommandType.IndexOf(" ")));
                int height = Convert.ToInt32(info.CommandType.Substring(info.CommandType.IndexOf(" "), info.CommandType.IndexOf(" ",0,2)));
                short depth = Convert.ToInt16(info.CommandType.Substring(info.CommandType.IndexOf(" ",0,2), info.CommandType.Length));
                WindowsInterface.setResolution(width,height,depth);
                */
                WindowsInterface.setResolution(info.CommandValue);
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.SystemBasic.ToString())
            {
                WindowsInterface.consoleToolExecute(info.CommandValue);
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.TouchpadButton.ToString())
            {
                WindowsInterface.sendTouchpadButton(info.CommandValue);
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.TouchpadWheel.ToString())
            {
                WindowsInterface.sendTouchpadWheel(Convert.ToInt32(info.CommandValue));
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.VoiceBot.ToString())
            {
                WindowsInterface.sendInputMessage(info.CommandValue);
                return;
            }
            if (info.CommandType == CommandInfoLib.CommandType.Speak.ToString())
            {
                WindowsInterface.sendOutputMessage(info.CommandValue);
                return;
            }
        }
    }
}
